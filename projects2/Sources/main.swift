import Foundation
import Kitura
import SwiftyJSON
import CouchDB

import HeliumLogger
import LoggerAPI

HeliumLogger.use()

let connectionProperties = ConnectionProperties(host: "localhost", port: 5984, secured: false)
let client = CouchDBClient(connectionProperties: connectionProperties)
let database = client.database("polls")

let router = Router()

router.get("/polls/list"){
    request, response, next in
    database.retrieveAll(includeDocuments: true){ docs, error in
        defer { next()}
        
        if let error = error{
            let errorMessage = error.localizedDescription
            let status = ["status": "error", "message": errorMessage]
            let result = ["result": status]
            let json = JSON(result)
            response.status(.OK).send(json: json)
        }else{
            let status = ["status": "ok"]
            var polls = [[String: Any]]()
            if let docs = docs {
                for document in docs["rows"].arrayValue {
                    var poll = [String: Any]()
                    poll["id"] = document["id"].stringValue
                    poll["title"] = document["doc"]["title"].stringValue
                    poll["option1"] = document["doc"]["option1"].stringValue
                    poll["option2"] = document["doc"]["option2"].stringValue
                    poll["votes1"] = document["doc"]["votes1"].intValue
                    poll["votes2"] = document["doc"]["votes2"].intValue
                    polls.append(poll)
                }
            }
            let result: [String: Any] = ["result": status, "polls": polls]
            let json = JSON(result)
            response.status(.OK).send(json: json)
        }
    
        
    }
}

extension String {
    func removingHTMLEncoding()-> String{
        let result = self.replacingOccurrences(of: "+", with: " ")
        return result.removingPercentEncoding ?? result
    }
}
router.post("/polls/create", middleware: BodyParser())
router.post("/polls/create"){
    request, response, next in
    
    // 2: check we have some data submitted
    guard let values = request.body else {
        try response.status(.badRequest).end()
        return
    }
    
    // 3: attempt to pull out URL-encoded values from the submission
    guard case .urlEncoded(let body) = values else {
        try response.status(.badRequest).end()
        return
    }
    
    // 4: create an array of fields to check
    let fields = ["title", "option1", "option2"]
    
     // this is where we'll store our trimmed values
    var poll = [String: Any]()
    
    for field in fields {
        if let value = body[field]?.trimmingCharacters(in: .whitespacesAndNewlines){
            
            // make sure it has at least 1 character
            if value.characters.count > 0{
                
                // add it to our list of parsed values
                poll[field] = value.removingHTMLEncoding()
                
                // important: this value exists, so go on to the next one
                continue
            }
        }
        
        // this value does not exist, so send back an error and exit
        
        try response.status(.badRequest).end()
        return
    }
    // fill in default values for the vote counts
    poll["vote1"] = 0
    poll["vote2"] = 0
    
    let json = JSON(poll)
    
    database.create(json){ id, revision, doc, error in
        defer { next() }
        
        if let id = id {
            let status = ["status": "ok", "id": id]
            let result = ["result": status]
            let json = JSON(result)
            response.status(.OK).send(json: json)
        }else{
            let errorMessage = error?.localizedDescription ?? "Unnown Error"
            let status = ["status": "error", "message": errorMessage]
            let result = ["result": status]
            let json = JSON(result)
            response.status(.internalServerError).send(json: json)
        }
    }
}

router.post("/polls/vote/:pollid/:option"){
    request, response, next in
    
    //ensure both parameters have values
    guard let poll = request.parameters["pollid"],
        let option = request.parameters["option"] else {
            try response.status(.badRequest).end()
            return
    }

    
    // attempt to pull out the poll the user requested
    database.retrieve(poll){ doc, error in
        if let error = error {
            // something went wrong!
            let errorMessage = error.localizedDescription
            let status = ["status": "error", "message":
                errorMessage]
            let result = ["result": status]
            let json = JSON(result)
            Log.debug("エラー")
            response.status(.notFound).send(json: json)
            next()
        }else if let doc = doc {
            // update the document
            Log.debug("通った1")
            var newDocument = doc
            let id = doc["_id"].stringValue
            let rev = doc["_rev"].stringValue
            Log.debug("通った2")
            if option == "1"{
                newDocument["vote1"].intValue += 1
            }else if option == "2"{
                newDocument["vote2"].intValue += 1
            }
            
            Log.debug("通った3")
            database.update(id, rev: rev, document: newDocument){ rev, doc, error in
                defer { next() }
                Log.debug("通った4")
                if let error = error {
                    let status = ["status": "error"]
                    let result = ["result": status]
                    let json = JSON(result)
                    Log.debug("通った5")
                    response.status(.conflict).send(json: json)
                }else {
                    Log.debug("通った6")
                    let status = ["status": "ok"]
                    let result = ["result": status]
                    let json = JSON(result)
                    response.status(.OK).send(json: json)
                }
            } //database.update
        }
    }
}
// set server on port 8090
Kitura.addHTTPServer(onPort: 8090, with: router)

// run server
Kitura.run()








